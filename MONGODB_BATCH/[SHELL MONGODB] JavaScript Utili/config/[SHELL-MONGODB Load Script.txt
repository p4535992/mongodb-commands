#INSIDE THE SHELL MONGO
load("C:\Users\Marco\Desktop\MONGODB_BATCH\Code\hour02\shell_script.js")
load("C:\D 2015-05-04\[NOSQL][RDFSTORE]\MONGODB_BATCH\MONGODB_BATCH\[SHELL MONGODB] JavaScript Utili\db_create.js")
#... PROMPT COMMAND
cd "C:\Users\Marco\Desktop\MONGODB_BATCH\Code\hour02\"
mongo shell_script.js

#UTILIZZO DI EVAL SUL SINGOLO COMANDO JAVASCRIPT
--eval "printjson(db.getCollectionNames());"

#MOSTRA TUTTI I DATABASE
--show dbs
--show users

#USA UN DETRMINATO DATABASE
use nome_database

#SHUT DOWN THE MONGODB SERVER
use admin 
db.shutdownServer() 
exit
