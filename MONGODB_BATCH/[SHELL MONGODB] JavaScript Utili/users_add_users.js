mongo = new Mongo("localhost");
db = mongo.getDB("users")
db.createUser(
			{
			user: "tenti",
            pwd: "tenti",
            roles: [ "dbAdmin" ]
			}
)
/*
//addUser è deprecato usa createUser
db.addUser( { user: "marco",
              pwd: "marco",
              roles: [ "dbAdmin" ] } );

db.addUser( { user: "testWriter",
              pwd: "test",
              roles: [ "readWrite" ] } );
db.addUser( { user: "testReader",
              pwd: "test",
              roles: [ "read" ] } );
db = mongo.getDB("admin")
db.addUser( { user: "testUser",
              userSource: "test",
              roles: [ "read" ],
              otherDBRoles: { test: [ "readWrite" ] } } );
*/
/*
use reporting
db.createUser(
    {
      user: "reportsUser",
      pwd: "12345678",
      roles: [
         { role: "read", db: "reporting" },
         { role: "read", db: "products" },
         { role: "read", db: "sales" },
         { role: "readWrite", db: "accounts" }
      ]
    }
)
*/